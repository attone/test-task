export const CREATE_TASK = 'CREATE_TASK';
export const CHANGE_TASK = 'CHANGE_TASK';
export const DELETE_TASK = 'DELETE_TASK';

export const CREATE_COLUMN = 'CREATE_COLUMN';
export const RENAME_COLUMN = 'RENAME_COLUMN';
