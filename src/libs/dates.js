const options = {
  month: 'long',
  day: 'numeric',
  hour: '2-digit',
  minute: 'numeric',
};

export const getDate = (date) => new Date(date).toLocaleTimeString('en-US', options);

export default {};
