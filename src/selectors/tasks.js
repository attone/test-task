export const getColumnTasks = (tasks, id) => tasks.filter((item) => item.status === id);

export const getTaskUser = (users, performer) => users.find(({ id }) => id === performer);
