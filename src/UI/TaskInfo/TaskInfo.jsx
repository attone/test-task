import React from 'react';
import PropTypes from 'prop-types';

import User from '../User/User';
import styles from './TaskInfo.module.scss';

const TaskInfo = ({ name, description, user }) => (
  <>
    <h3 className={styles.title}>{name}</h3>
    <p className={styles.description}>
      {description}
    </p>
    <User avatar={user.avatar} name={user.name} />
  </>
);

TaskInfo.defaultProps = {
  name: '',
  description: '',
};

TaskInfo.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default TaskInfo;
