import React from 'react';
import PropTypes from 'prop-types';

import styles from './User.module.scss';

const User = ({ name, avatar }) => (
  <div className={styles.user}>
    <div className={styles.avatar} style={{ background: `${avatar}` }} />
    <span className={styles.name}>{name}</span>
  </div>
);

User.propTypes = {
  name: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
};

export default User;
