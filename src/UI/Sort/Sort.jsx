import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Sort.module.scss';

const Sort = ({ sort, onChange }) => (
  <div className={styles.sort}>
    <span className={styles.sortTitle}>Sort by:</span>
    <button
      type="button"
      onClick={onChange}
      value="date"
      className={classNames(styles.dateSortButton, { [`${styles.isActive}`]: sort === 'date' })}
    >
      date
    </button>
    <button
      type="button"
      onClick={onChange}
      value="name"
      className={classNames(styles.nameSortButton, { [`${styles.isActive}`]: sort === 'name' })}
    >
      name
    </button>
  </div>
);

Sort.propTypes = {
  sort: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Sort;
