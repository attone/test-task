import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styles from './EditableText.module.scss';

class EditableText extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false,
      editableText: '',
    };
  }

  handleEdit = () => {
    const { text } = this.props;
    this.setState({
      isEdit: true,
      editableText: text,
    });
  }

  handleChangeName = ({ target }) => {
    const { value } = target;
    this.setState({ editableText: value });
  }

  handleSaveName = () => {
    const { onChange } = this.props;
    const { editableText } = this.state;
    this.setState({ isEdit: false });
    onChange(editableText);
  }

  render() {
    const { text } = this.props;
    const { isEdit, editableText } = this.state;

    return (
      <div className={styles.form}>
        {
          isEdit
            ? <input className={styles.input} type="text" value={editableText} onChange={this.handleChangeName} onBlur={this.handleSaveName} />
            : <span className={styles.text}>{text}</span>
        }
        <button
          type="button"
          className={styles.button}
          onClick={isEdit ? this.handleSaveName : this.handleEdit}
        >
          {isEdit ? 'Save' : 'Edit'}
        </button>
      </div>
    );
  }
}

EditableText.propTypes = {
  text: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default EditableText;
