import React from 'react';
import PropTypes from 'prop-types';

import styles from './CreateButton.module.scss';

const CreateButton = ({ onClick }) => (
  <button className={styles.button} type="button" onClick={onClick}>
    +
  </button>
);

CreateButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default CreateButton;
