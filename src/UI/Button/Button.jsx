import React from 'react';
import PropTypes from 'prop-types';

import styles from './Button.module.scss';

const Button = ({ onChange, children }) => (
  <button className={styles.button} type="button" onClick={onChange}>
    {children}
  </button>
);

Button.defaultProps = {
  children: null,
};

Button.propTypes = {
  children: PropTypes.node,
  onChange: PropTypes.func.isRequired,
};

export default Button;
