import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './TaskEdit.module.scss';

class TaskEdit extends PureComponent {
  handleChangeName = ({ target }) => {
    const { onChange } = this.props;
    const { value } = target;
    onChange('name', value);
  }

  handleChangeDescription = ({ target }) => {
    const { onChange } = this.props;
    const { value } = target;
    onChange('description', value);
  }

  handleChangePerformer = ({ target }) => {
    const { onChange } = this.props;
    const { value } = target;
    onChange('performer', value);
  }

  render() {
    const {
      name, description, performer, users,
    } = this.props;

    return (
      <>
        <input
          className={styles.name}
          type="text"
          value={name}
          onChange={this.handleChangeName}
        />
        <textarea
          className={styles.description}
          onChange={this.handleChangeDescription}
          value={description}
        />
        <select
          className={styles.performer}
          onChange={this.handleChangePerformer}
          value={performer}
        >
          {users.map((user) => <option key={`userSelect_${user.id}`} value={user.id}>{user.name}</option>)}
        </select>
      </>
    );
  }
}

TaskEdit.defaultProps = {
  name: '',
  description: '',
};

TaskEdit.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  performer: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }).isRequired).isRequired,
  onChange: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  users: state.users,
});

export default connect(mapStateToProps)(TaskEdit);
