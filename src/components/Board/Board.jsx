import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-beautiful-dnd';
import { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';

import { createColumn, changeTask } from 'actions';
import Column from 'components/Column/Column';
import { Button } from 'UI';


import styles from './Board.module.scss';

class Board extends PureComponent {
  handleDragEnd = ({ draggableId, destination, source }) => {
    const { changeTask: handleChangeTask } = this.props;

    if ((!source || !destination) || source.droppableId === destination.droppableId) return;

    handleChangeTask({
      id: draggableId,
      status: destination.droppableId,
    });

    if (destination.droppableId === '4') {
      store.addNotification({
        title: 'Wonderful!',
        message: 'The task is done!',
        type: 'success',
        insert: 'top',
        container: 'top-right',
        dismiss: {
          duration: 5000,
          onScreen: true,
        },
        width: 300,
      });
    }
  }

  render() {
    const { columns, createColumn: handleCreateColumn } = this.props;

    return (
      <div className={styles.container}>
        <DragDropContext onDragEnd={this.handleDragEnd}>
          {columns.map(({ id, name }) => (
            <Column
              key={`column_${id}`}
              id={id}
              name={name}
            />
          ))}
        </DragDropContext>
        <div className={styles.addColumn}>
          <Button onChange={handleCreateColumn}>
            Create column
          </Button>
        </div>
      </div>
    );
  }
}

Board.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    sort: PropTypes.number,
  }).isRequired).isRequired,
  createColumn: PropTypes.func.isRequired,
  changeTask: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  columns: state.columns,
});

export default connect(
  mapStateToProps,
  { createColumn, changeTask },
)(Board);
