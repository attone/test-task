/* eslint-disable react/jsx-props-no-spreading */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Draggable } from 'react-beautiful-dnd';

import { changeTask, deleteTask } from 'actions';
import { getTaskUser } from 'selectors/tasks';
import { getDate } from 'libs/dates';
import { TaskInfo, TaskEdit } from 'UI';

import styles from './Task.module.scss';

export const getBorderColor = {
  1: 'gray',
  2: 'blue',
  3: 'red',
  4: 'blue',
} || 'gray';

class Task extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false,
      name: '',
      description: '',
      performer: '',
    };
  }

  handleEdit = () => {
    const { name, description, performer } = this.props;

    this.setState({
      isEdit: true,
      name,
      description,
      performer,
    });
  }

  handleChange = (field, value) => {
    this.setState({ [field]: value });
  }

  handleDelete = () => {
    const { id, deleteTask: onDelete } = this.props;
    onDelete(id);
  }

  handleSave = () => {
    const { id, changeTask: handleChangeTask } = this.props;
    const { name, description, performer } = this.state;

    this.setState({
      isEdit: false,
    });

    handleChangeTask({
      id,
      name,
      description,
      performer,
    });
  }

  render() {
    const {
      name, description, status, user, createdAt, id, index,
    } = this.props;
    const {
      isEdit, name: stateName, description: stateDescription, performer: statePerformer,
    } = this.state;

    return (
      <Draggable
        draggableId={String(id)}
        index={index}
      >
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            className={
              classNames(styles.container, { [`${styles.isDragging}`]: snapshot.isDragging }, styles[getBorderColor[status]])
            }
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            {isEdit
              ? (
                <TaskEdit
                  name={stateName}
                  description={stateDescription}
                  performer={statePerformer}
                  onChange={this.handleChange}
                />
              ) : <TaskInfo name={name} description={description} user={user} />}

            <div className={styles.footer}>
              {isEdit
                ? (
                  <>
                    <button type="button" className={styles.editButton} onClick={this.handleDelete}>
                      Delete
                    </button>
                    <button type="button" className={styles.editButton} onClick={this.handleSave}>
                      Save
                    </button>
                  </>
                )
                : (
                  <>
                    <span className={styles.date}>
                      {getDate(createdAt)}
                    </span>
                    <button type="button" className={styles.editButton} onClick={this.handleEdit}>
                      Edit
                    </button>
                  </>
                )}
            </div>
          </div>
        )}
      </Draggable>
    );
  }
}

Task.defaultProps = {
  name: '',
  description: '',
};

Task.propTypes = {
  name: PropTypes.string,
  description: PropTypes.string,
  status: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  performer: PropTypes.string.isRequired,
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
  }).isRequired,
  changeTask: PropTypes.func.isRequired,
  deleteTask: PropTypes.func.isRequired,
};


const mapStateToProps = (state, props) => ({
  user: getTaskUser(state.users, props.performer) || {},
});

export default connect(
  mapStateToProps,
  { changeTask, deleteTask },
)(Task);
