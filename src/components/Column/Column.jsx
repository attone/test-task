import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Droppable } from 'react-beautiful-dnd';
import classNames from 'classnames';

import { renameColumn, createTask } from 'actions';
import { getColumnTasks } from 'selectors/tasks';
import { EditableText, CreateButton, Sort } from 'UI';

import Task from './Task/Task';
import styles from './Column.module.scss';

class Column extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      sort: 'date',
    };
  }

  handleSort = ({ target }) => {
    const { value } = target;
    this.setState({ sort: value });
  }

  handleSave = (name) => {
    const { id, renameColumn: onRename } = this.props;
    onRename(id, name);
  }

  handleCreateTask = () => {
    const { id, createTask: onCreateTask } = this.props;
    onCreateTask({
      name: 'New task',
      description: 'No description',
      performer: '1',
      status: id,
    });
  }

  getSortedTasks = () => {
    const { sort } = this.state;
    const { tasks } = this.props;

    if (!tasks.length) return [];

    return tasks.sort((a, b) => {
      if (sort === 'date') return new Date(a.createdAt) - new Date(b.createdAt);
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      return 0;
    });
  }

  render() {
    const { name, id } = this.props;
    const { sort } = this.state;

    const tasks = this.getSortedTasks();

    return (
      <Droppable droppableId={id}>
        {(provided, snapshot) => (
          <div
            className={styles.container}
          >
            <div className={styles.title}>
              <EditableText text={name} onChange={this.handleSave} />
            </div>
            <div
              ref={provided.innerRef}
              className={classNames(styles.column, { [`${styles.active}`]: snapshot.isDraggingOver })}
            >
              <Sort sort={sort} onChange={this.handleSort} />
              {tasks.length > 0 && tasks.map((item, index) => (
                <Task
                  key={`task_${item.id}`}
                  name={item.name}
                  id={item.id}
                  index={index}
                  description={item.description}
                  performer={item.performer}
                  status={item.status}
                  createdAt={item.createdAt}
                />
              ))}
              {!tasks.length && !snapshot.isDraggingOver && (
                <span>Haven`t tasks yet</span>
              )}
              {snapshot.isDraggingOver
                ? provided.placeholder
                : <CreateButton onClick={this.handleCreateTask} />}
            </div>
          </div>
        )}
      </Droppable>
    );
  }
}

Column.defaultProps = {
  tasks: [],
};

Column.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  renameColumn: PropTypes.func.isRequired,
  createTask: PropTypes.func.isRequired,
  tasks: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    performer: PropTypes.string,
    status: PropTypes.string,
  }).isRequired),
};

const mapStateToProps = (state, props) => ({
  tasks: getColumnTasks(state.tasks, props.id),
});

export default connect(
  mapStateToProps,
  { renameColumn, createTask },
)(Column);
