// eslint-disable-next-line no-unused-vars
import { CREATE_TASK, CHANGE_TASK, DELETE_TASK } from 'constants/actions';

export const createTask = (task) => ({
  type: CREATE_TASK,
  ...task,
});

export const changeTask = (task) => ({
  type: CHANGE_TASK,
  ...task,
});

export const deleteTask = (id) => ({
  type: DELETE_TASK,
  id,
});
