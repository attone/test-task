// eslint-disable-next-line no-unused-vars
import { CREATE_COLUMN, RENAME_COLUMN } from 'constants/actions';

export const createColumn = () => ({
  type: CREATE_COLUMN,
});

export const renameColumn = (id, name) => ({
  type: RENAME_COLUMN,
  id,
  name,
});
