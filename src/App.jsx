import React from 'react';
import ReactNotification from 'react-notifications-component';

import Board from './components/Board/Board';

import './App.css';

function App() {
  return (
    <div className="App">
      <ReactNotification />
      <Board />
    </div>
  );
}

export default App;
