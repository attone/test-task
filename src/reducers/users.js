const defaultState = [
  {
    name: 'Eesha Humphrey',
    avatar: '#AFB42B',
    id: '1',
  },
  {
    name: 'Shanae Cardenas',
    avatar: '#5D4037',
    id: '2',
  },
  {
    name: 'Elvis Guest',
    avatar: '#7C4DFF',
    id: '3',
  },
  {
    name: 'Aviana Compton',
    avatar: '#303F9F',
    id: '4',
  },
  {
    name: 'Dylan Lacey',
    avatar: '#FFC107',
    id: '5',
  },
];

const users = (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default users;
