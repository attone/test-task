import { CREATE_COLUMN, RENAME_COLUMN } from 'constants/actions';

const defaultState = [
  {
    name: 'Backlog',
    id: '1',
    isNotify: false,
  },
  {
    name: 'Todo',
    id: '2',
    isNotify: false,
  },
  {
    name: 'In progress',
    id: '3',
    isNotify: false,
  },
  {
    name: 'Done',
    id: '4',
    isNotify: true,
  },
];

const getLast = (state, field) => state.reduce(
  (a, b) => (a[field] > b[field] ? a[field] : b[field]), 0,
);

const columns = (state = defaultState, action) => {
  switch (action.type) {
    case CREATE_COLUMN:
      return [
        ...state,
        {
          name: '',
          isNotify: false,
          id: `${Number(getLast(state, 'id')) + 1}`,
        },
      ];
    case RENAME_COLUMN:
      return state.map((item) => (item.id === action.id ? {
        ...item,
        name: action.name,
      } : item));
    default:
      return state;
  }
};

export default columns;
