import { CREATE_TASK, CHANGE_TASK, DELETE_TASK } from 'constants/actions';

const defaultProps = [
  {
    name: 'Default backlog task',
    description: 'Task description',
    performer: '1',
    status: '1',
    id: '0',
    createdAt: 'Thu, 03 Oct 2019 22:27:36 GMT',
  },
  {
    name: 'Qefault 456 task',
    description: 'Task description',
    performer: '1',
    status: '1',
    id: '1',
    createdAt: 'Thu, 02 Oct 2019 22:27:36 GMT',
  },
];

const getLast = (state, field) => state.reduce(
  (a, b) => (a[field] > b[field] ? a[field] : b[field]), 0,
);

export const getTask = (state, id) => state.find((item) => item.id === id);

const tasks = (state = defaultProps, action) => {
  const currentTask = getTask(state, action.id);
  switch (action.type) {
    case CREATE_TASK:
      return [
        ...state,
        {
          id: `${Number(getLast(state, 'id')) + 1}`,
          status: action.status,
          description: action.description,
          performer: action.performer,
          name: action.name,
          createdAt: new Date().toLocaleString(),
        },
      ];
    case CHANGE_TASK:
      return state.map((item) => (item.id === action.id
        ? {
          ...currentTask,
          ...action,
        }
        : item));
    case DELETE_TASK:
      return state.filter(({ id }) => id !== action.id);
    default:
      return state;
  }
};

export default tasks;
