import { combineReducers } from 'redux';

import users from './users';
import tasks from './tasks';
import columns from './columns';

export default combineReducers({
  tasks,
  columns,
  users,
});
